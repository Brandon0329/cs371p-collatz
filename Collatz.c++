// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>   // assert
#include <iostream>  // endl, istream, ostream
#include <sstream>   // istringstream
#include <string>    // getline, string
#include <utility>   // make_pair, pair
#include <vector>    // vector
#include <algorithm> // max, min

#include "Collatz.h"

using namespace std;

// -----------
// get_lengths
// -----------

vector<int> get_lengths (int N) {
    assert(N > 0 && N <= 1000000);
    vector<int> lengths(N + 1);
    for(int i = 1; i <= N; ++i) {
        int count = 1;
        long long int temp = i;
        while(temp > 1) {
            if((temp & 1) == 0)
                temp >>= 1;
            else
                temp = temp * 3 + 1;
            if(temp <= N && lengths[(int) temp] > 0) {
                count += lengths[(int) temp];
                break;
            }
            count++;
        }
        lengths[i] = count;
    }
    return lengths;
}

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j, vector<int>& lengths) {
    // uses precomputed cycle lengths to find max cycle
    // length from i to j or j to i.
    int ans = 0;
    int left = min(i, j);
    int right = max(i, j);
    for(int k = left; k <= right; ++k)
        ans = max(ans, lengths[k]);
    return ans;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    vector<int> lengths = get_lengths(1000000);
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j, lengths);
        collatz_print(w, i, j, v);
    }
}
