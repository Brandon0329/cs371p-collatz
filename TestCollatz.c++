// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair
#include <vector>   // vector

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

// -----------------
// get_cycle_lengths
// -----------------

TEST(CollatzFixture, get_cycle_lengths_1) {
    vector<int> lengths = get_lengths(1);
    ASSERT_EQ(lengths[1], 1);
}

TEST(CollatzFixture, get_cycle_lengths_2) {
    vector<int> lengths = get_lengths(9);
    ASSERT_EQ(lengths[9], 20);
}

TEST(CollatzFixture, get_cycle_lengths_3) {
    vector<int> lengths = get_lengths(3711);
    ASSERT_EQ(lengths[3711], 238);
}

TEST(CollatzFixture, get_cycle_lengths_4) {
    vector<int> lengths = get_lengths(1000000);
    ASSERT_EQ(lengths[837799], 525);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    vector<int> lengths = get_lengths(1000000);
    const int v = collatz_eval(1, 10, lengths);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    vector<int> lengths = get_lengths(1000000);
    const int v = collatz_eval(100, 200, lengths);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    vector<int> lengths = get_lengths(1000000);
    const int v = collatz_eval(201, 210, lengths);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    vector<int> lengths = get_lengths(1000000);
    const int v = collatz_eval(900, 1000, lengths);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval_5) {
    vector<int> lengths = get_lengths(1000000);
    const int v = collatz_eval(8, 294849, lengths);
    ASSERT_EQ(v, 443);
}

TEST(CollatzFixture, eval_6) {
    vector<int> lengths = get_lengths(1000000);
    const int v = collatz_eval(483, 185, lengths);
    ASSERT_EQ(v, 144);
}

TEST(CollatzFixture, eval_7) {
    vector<int> lengths = get_lengths(1000000);
    const int v = collatz_eval(104827, 428, lengths);
    ASSERT_EQ(v, 351);
}

TEST(CollatzFixture, eval_8) {
    vector<int> lengths = get_lengths(1000000);
    const int v = collatz_eval(152, 107, lengths);
    ASSERT_EQ(v, 122);
}

TEST(CollatzFixture, eval_9) {
    vector<int> lengths = get_lengths(1000000);
    const int v = collatz_eval(9, 9, lengths);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_10) {
    vector<int> lengths = get_lengths(1000000);
    const int v = collatz_eval(810518, 560705, lengths);
    ASSERT_EQ(v, 509);
}

TEST(CollatzFixture, eval_11) {
    vector<int> lengths = get_lengths(1000000);
    const int v = collatz_eval(6, 301468, lengths);
    ASSERT_EQ(v, 443);
}


// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}
